import nose 
import logging 
import arrow
from acp_times import open_time, close_time

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def test_open():
    """
    Testing Open Time calculator Function
    https://rusa.org/pages/acp-brevet-control-times-calculator
    """
    starttime = arrow.get(2020,1,1)

    open_ins= [
    [60,200],[200,200],[550,600],[1150,1000], [620,600]            
    ]

    expected_list = [
        [1,46],[5,53],[17,8],[33,5], [18,48]
    ]
    passed = True
    for i in range(4):
        opentime = open_time(open_ins[i][0],open_ins[i][1],starttime)
        expected = starttime.shift(hours=expected_list[i][0], minutes=expected_list[i][1]).isoformat()
        print(opentime)
        print(expected)
        print("")
        if (opentime != expected):
            print(f"{opentime} and {expected} are not equal!" )
            passed = False

    assert passed
    

def test_close():
    """
    Nosetest for close times using provided examples from 
    https://rusa.org/pages/acp-brevet-control-times-calculator
    """

    starttime = arrow.get(2020,1,1)

    open_ins= [
        [550,600],[890,1000], [620,600]            
    ]

    expected_list = [
        [36,40],[65,23], [40,0]
    ]
    passed = True
    for i in range(len(open_ins)):
        closetime = close_time(open_ins[i][0],open_ins[i][1],starttime)
        expected = starttime.shift(hours=expected_list[i][0], minutes=expected_list[i][1]).isoformat()
        print(close_time)
        print(expected)
        print("")
        if (closetime != expected):
            print(f"{closetime} and {expected} are not equal!" )
            passed = False

    assert passed



test_open()
test_close()
