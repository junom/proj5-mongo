"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import os 
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import json
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
# intermediate dictionary to hold inputs before submitted into databased
# TODO: put this on frontend to save on storage
entrydict = dict()

# needed declarations for the DataBase
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.entrydb
db.entrydb.delete_many({ })
starttime = 0
startdate = 0


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    miles = request.args.get('miles', 999, type=float)

    #these two bois are global now CAUTION!! 
    starttime = request.args.get("starttime", 999,type=str)
    startdate = request.args.get("startdate",999,type=str)

    brevit_dist = request.args.get("brevdist",999,type=int)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))


    # need to make the starttime and date into arrow object 
    start = arrow.get(startdate + " " + starttime)

    app.logger.debug(f"Merged date is {start}")
    open_time = acp_times.open_time(km, brevit_dist, start)
    app.logger.info(f"Open Time: {open_time}")
    close_time = acp_times.close_time(km, brevit_dist, start)
    app.logger.info(f"Close Time: {close_time}")

    result = {"open": open_time, "close": close_time}
    retval = flask.jsonify(result=result)

    #put the stuff we want to display in our final website
    result.update({"miles": miles, "km": km,"brevit_dist":brevit_dist})
    entrydict.update(result)
    return retval


@app.route("/submit")
def _submit_to_db():
    """
    stores the entrylist of json objects in a database
    """
    result = flask.request.args.get('tableData', 999,type=str)
    # clear old entries 
    db.entrydb.delete_many({ })
    list_o_dicts = json.loads(result)
    for item in list_o_dicts:
        print(item)
    # print(f"Gonna submit:")
    # print(entrydict)
    # print(type(entrydict))
    db.entrydb.insert_many(list_o_dicts)
    # clear the dict so we dont get duplicates 

    return "it doesnt actually matter"



@app.route("/display")
def _display_all_entries():
    """
    Display the Entries on a new htmlpage my guy
    """
    print("We in DISPLAY")
    # get all items from database
    _items = db.entrydb.find()
    starttime = request.args.get("starttime", 999,type=str)
    print(starttime)
    startdate = request.args.get("startdate",999,type=str)
    print(startdate)
    brevit_dist = request.args.get("brevdist",999,type=int)
    print(brevit_dist)
    # put into python list to be given to flask template
    items =[item for item in _items]

    if len(items) == 0:
        return flask.render_template("oops.html")

    print(items)

    # return flask.redirect("http://localhost:5000/display.html", code=200)
    
    return flask.render_template('display.html', items=items, startdate=startdate, starttime=starttime, brevit_dist= brevit_dist)

    # return flask.render_template('display.html',items=items, startdate=startdate, starttime=starttime)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.INFO)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
